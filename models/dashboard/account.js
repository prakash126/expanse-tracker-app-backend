const mongoose = require('mongoose');

const account = mongoose.Schema({
    userId:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"user"
    },
    name:{type:String,required:true},
    balance:{type:Number,default:0},
   addUser:[{type:String,default:null}]
  
});

module.exports = mongoose.model("account",account);