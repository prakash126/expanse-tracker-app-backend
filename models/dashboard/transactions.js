const mongoose = require('mongoose');

const transactions = mongoose.Schema({
    userId:mongoose.Schema.Types.ObjectId,
    transactionType:String,
    amount:Number,
    timeStamp: {type:Date,default:Date.now},
    categoryName:String,
    transfer: {
        accountId:{type:mongoose.Schema.Types.ObjectId,default:null},
        accountName:{type:String,default:null},
        to: {type:String,default:null},
        from: {type:String,default:null},
        amount:{type:Number,default:0},
        userId:{type:mongoose.Schema.Types.ObjectId,default:null}
      },
      accountName:{type:String}
});

module.exports = mongoose.model("transactions",transactions);