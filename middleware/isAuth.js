const jwt = require("jsonwebtoken");
const User = require("../models/authuser")
module.exports = (req, res, next) => {
//     console.log(req.headers.authorization)
//   const token = req.headers.authorization.split(" ")[1];
//console.log(req.headers);

  const {authorization} = req.headers
  //console.log(authorization)
  if(!authorization){
    return res.status(422).json({error:"Please Login Again! "});
  }
  const token = authorization.replace("Bearer ","")
 // console.log(token);
  jwt.verify(token, process.env.PRIVATE_KEY, (err, decoded) => {
    if (err) {
      return res.status(422).json({ error: "auth_error" });
    } else {
      req.user = {
        _id: decoded._id,
        name: decoded.name,
        email: decoded.email,
      };
      next();
    }
  });
};
