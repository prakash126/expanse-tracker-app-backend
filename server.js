const express = require("express");
const app = express();

const mongoose = require("mongoose");
const cors = require("cors");
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

require("dotenv").config();

const port = process.env.PORT;


app.use(express.json());

const authRoutes = require('./routes/authuser');
const dashboardRoutes = require('./routes/dashboard/detailsAccounAndCategory')
const transactionRoutes = require("./routes/dashboard/transactions");
const accountRoutes = require("./routes/dashboard/account");
const isAuth = require("./middleware/isAuth");
app.use("/api/auth",authRoutes);
app.use("/api/dashboard",isAuth,dashboardRoutes);
app.use("/api/transaction",isAuth,transactionRoutes);
app.use("/api/accountsOperation",isAuth,accountRoutes);

mongoose.connect(process.env.MONGODBURI,{
    useNewUrlParser:true,
    useUnifiedTopology:true
}).then(()=>{
    console.log("DATABASE CONNECTED!!!!!");
    app.listen(port,()=>{
        console.log(`APP BACKEND IS RUNNUIN AT ${port}`)
    });
})
.catch((err)=>{
    console.log(err);
})
 
