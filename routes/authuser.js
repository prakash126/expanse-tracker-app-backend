const express = require('express');
const router = express.Router();
const authController = require('../controller/authuser');

router.post('/signup',authController.signup);
router.post('/signin',authController.sigin);
router.post('/getUserDetails',authController.getUser);
module.exports = router;