const express = require("express");
const router = express.Router();
const accountController  =  require("../../controller/dashboard/account");
router.put("/updateAccount",accountController.updateAccountDetails);
router.post("/addAccount",accountController.addAccount);
router.post("/deleteAccount",accountController.deleteAccount);
router.post("/addUserToAccountByEmail",accountController.addUserToAccount);
router.post("/getAddedUser",accountController.getAddedUser);
module.exports = router;