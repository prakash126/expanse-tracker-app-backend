const express = require('express');
const router = express.Router();

const dashboardController = require('../../controller/dashboard/detailsAccounAndCategory')

router.get('/getAllAccountDetails',dashboardController.getAllAccountDetails);
router.post('/accountDetailBasedOnUserId',dashboardController.accountDetails);
router.post('/categoryDetailBasedOnUserId',dashboardController.categoryDetails);
router.post('/accountDetailsBasedOnAccountId',dashboardController.accountDetailsBasedOnAccountId)
module.exports = router;

