const express = require("express");
const router = express.Router();
const transactionController = require('../../controller/dashboard/transactions');

router.post('/getTransactionDetails',transactionController.getTransactions);
router.post('/getTransDetlsBsdOnAcNameAndUid',transactionController.getTransDetlsBsdOnAcNameAndUid)
router.post('/transactionpostDetails',transactionController.transactionDetails);
router.put('/transferMoney',transactionController.transferMoney);
router.put('/editTransaction',transactionController.editTransaction)
router.post('/deleteTransaction',transactionController.deleteTransaction)
module.exports = router;