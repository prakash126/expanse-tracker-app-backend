const User = require('../models/authuser');
const Account = require('../models/dashboard/account');
const Category = require("../models/dashboard/category");

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const mongoose = require('mongoose');
const nm = require('nodemailer');

transport=nm.createTransport({
    service:"gmail",
    auth:{
        user:"prakashkumar845411@gmail.com",
        pass:"Motihari@845411"
    }
})


exports.signup = (req,res)=>{
   console.log(req.body);
    User.find({email:req.body.email})
    .countDocuments()
    .then((count)=>{
        if(count>0){
            res.json({
                status:"error",
                msg:"email_already_resgissterd",
            });
        }
        else if(req.body.password !== req.body.confirmPassword){
            res.json({
                status: "error",
                msg: "passwords_do_not_match",
              });
        }
        else{
            bcrypt.hash(req.body.password,12,(err,hash)=>{
                if(err){
                    return res.json({
                        status:"error",
                        msg:err
                    });
                }
                const user = new User({
                    name:req.body.name,
                    email:req.body.email,
                    password:hash
                });
                user.save().then((user)=>{
                    const account = new Account({
                        userId:mongoose.Types.ObjectId(user._id),
                        name:"CASH",
                    });
                    account.save();
                    const category = new Category({
                        userId:mongoose.Types.ObjectId(user._id),
                        name:"Default",
                    });
                    category.save();
                    
                    var mailcontent = "Welcome To Expanse App " + user.name;
                    transport.sendMail(
                        {
                            to:user.email,
                            subject:"Greeting Message",
                            html:mailcontent,
                            from:"prakashkumar845411@gmail.com"
                        },
                        function(err,result){
                            if(err){
                                console.log(err);
                            }
                            else{
                                console.log(result);
                            }
                        }
                    )

                    res.json({
                        status: "ok",
                        msg: "user_created",
                    });
                    }).catch((err)=>{
                        res.json({
                            status:"error",
                            msg:"user not created"
                        })
                    })
            })
        }
    })
}

exports.sigin = (req,res)=>{
    // console.log(req.body);
    // res.json({
    //     status:"status",
    //     data:req.body
    // })
    User.findOne({email:req.body.email}).then((user)=>{
        if(!user){
           return res.json({
                status:'error',
                message:"Email Not Exists"
            })
        }
        bcrypt.compare(req.body.password,user.password,(err,isMatched)=>{
            if(!isMatched){
                return res.json({
                    status:'error',
                    message:'Invalid Passsword'
                })
            }

            const token = jwt.sign(
                {
                    _id:user.id,
                    name:user.name,
                    email:user.email
                },
                process.env.PRIVATE_KEY,
                {
                    expiresIn:"1h",
                }
            );
          
            res.json({
                status:"ok",
                message:"Login Success",
                user:user,
                token:token
            })
        });
    });
};

exports.getUser = (req,res)=>{
    User.findById(req.body.id).then((data)=>{
        if(data.length === 0){
           return res.status(400).json({
                err:"User Data Not Found"
            })
        }
        res.send({user:data});
    }).catch((err)=>{
        res.send({err:err});
    })
}