const User = require('../../models/authuser');
const Account = require('../../models/dashboard/account');
const Category = require("../../models/dashboard/category");
const Transaction = require("../../models/dashboard/transactions")
const mongoose = require('mongoose');

exports.transactionDetails = (req,res)=>{
  // console.log(req.body)
   const userId = req.body.userId;
   const obj = {
       name:req.body.accounName,
       
       transactionType:req.body.transactionType,
       amount:req.body.amount,
       categoryName:req.body.categoryName
   }
  //console.log(obj)
   //Account.findByIdAndUpdate(userId,{name:obj.name,});
   const transaction = new Transaction({
       userId:userId,
       transactionType:obj.transactionType,
       amount:obj.amount,
       categoryName:obj.categoryName,
       accountName:obj.name
   })
   transaction.save().then((data)=>{
       //console.log(data);
       res.send(data);
   }).catch((err)=>{
       //console.log(err);
       res.send(err);
   })
}

exports.getTransactions = (req, res) => {
   Transaction.find({userId:req.body.userId}).then((data)=>{
     if(data.length===0){
       return res.status(400).json({
         errMsg:'No Transaction record found of this user'
       })
     }
     res.send(data);
   }).catch((err)=>{
     console.log(err);
     res.send(err);
   })
  };

  exports.getTransDetlsBsdOnAcNameAndUid = (req,res)=>{
    console.log(req.body)
    Transaction.find({userId:req.body.obj.userId,accountName:req.body.obj.accountName}).then((data)=>{
      if(data.length === 0){
        return res.send({err:"Transaction Details Not Found"})
      }
      res.send(data);
    }).catch((err)=>{
      res.send({err:err});
    })
  }

  // updateTransaction
  exports.editTransaction = (req,res)=>{
    // console.log(req.body)
    Transaction.findById(req.body.obj.id).then((data)=>{
      if(data.length === 0){
        return res.status(400).json({
          errMsg:"Transaction Details not Found"
        })
      }
      data.amount = req.body.obj.amount;
      data.save().then((dt)=>{
        res.send(dt);
      }).catch((err)=>{
        res.send(err);
      })
    })
  }

  // delete Transaction

  exports.deleteTransaction = (req,res)=>{
    console.log(req.body);
    Transaction.findOneAndDelete({_id:req.body.id}).then((data)=>{
      //console.log(data)
      if(data.length===0){
        return res.status(400).json({
          msg:"Not Deleted"
        })
      }
      res.status(200).json({
        msg:"DELETED"
      })
    })
  }

  // transfer money

  exports.transferMoney = (req,res)=>{
    // console.log("=============")
    // console.log(req.body);
    // console.log("=============")
    Account.findById(req.body.accountId).then((data)=>{
      if(data.length===0){
       return res.status(400).json({
          errMsg:"Account Not Found"
        })
      }
     // console.log(data)
    for(let addUser of data.addUser){
      if(req.body.to !== addUser){
        //console.log("nhi milgaya")
       return res.status(400).json({
          errMsg:"You are not able to transfer money beacuse other party doesn't exits in your group"
        })
      }
    }
    Transaction.find({userId:req.body.userId,accountName:req.body.accountName}).then((transactions)=>{
      //console.log(transactions)
      for(let dt of transactions){
        //console.log(dt.transfer.ref)
        dt.transfer.accountId=req.body.accountId;
        dt.transfer.accountName=req.body.accountName;
        dt.transfer.to=req.body.to,
        dt.transfer.from=req.body.from,
        dt.transfer.amount=req.body.amount,
        dt.transfer.userId=req.body.userId
        dt.save().then((data)=>{
          console.log(data);
          res.status(200).json({msg:"Transaction Successful",data:data})
        }).catch((err)=>{
          console.log(err)
        })
      }
     
    }).catch((err)=>{
      res.send(err)
    })
    })
  }