const User = require('../../models/authuser');
const Account = require('../../models/dashboard/account');
const Category = require("../../models/dashboard/category");

const mongoose = require('mongoose');

exports.getAllAccountDetails = (req,res)=>{
 
    Account.find().populate("userId")
    .then((data)=>{
        console.log(data)
        //res.json({accdtlsBasedOnUserId:JSON.stringify(data)})
        res.send(data);
    }).catch((err)=>{
        console.log(err);
        res.json({
            err:err
        })
    })
}

exports.accountDetails = (req,res)=>{
 
    Account.find({userId:req.body.userId})
    .then((data)=>{
        //console.log(data)
        //res.json({accdtlsBasedOnUserId:JSON.stringify(data)})
        res.send(data);
    }).catch((err)=>{
        //console.log(err);
        res.json({
            err:err
        })
    })
}

exports.accountDetailsBasedOnAccountId = (req,res)=>{
    console.log(req.body)
   // const account = [];
   Account.findById(req.body.id).then((data)=>{
       if(data.length === 0){
          return res.status(400).json({
               msg:"Account Not Found"
           })
       }
       console.log(data)
       res.send({account:data});
   }).catch((err)=>{
       console.log(err);
       res.send({err:err})
   })
}

exports.categoryDetails = (req,res)=>{
   // console.log(req.body);
    Category.find({userId:req.body.userID}).then((data)=>{
        //console.log(data);
        res.send(data);
    }).catch((err)=>{
        console.log(err);
        res.send(err);
    })
}