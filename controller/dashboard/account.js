const mongoose =  require('mongoose');
const Account = require("../../models/dashboard/account");
const User = require("../../models/authuser")
//add account

exports.addAccount = (req,res)=>{
   // console.log(req.body);
    const newAccount = new Account({
        userId:req.body.obj.userId,
        name:req.body.obj.name
    });
    newAccount.save().then((data)=>{
        console.log(data);
        res.send(data);
    }).catch((err)=>{
        console.log(err);
    })
}

// edit account
exports.updateAccountDetails = (req,res)=>{
    const {id,name} =  req.body.obj
    //console.log(name);
    Account.findById({_id:id}).then((account) => {
        if (!account) {
          res.status(404).json({
            status: "error",
            msg: "account_not_found",
          });
        } else {
          account.name = name;
          account.save().then((data)=>{
              res.send(data);
          }).catch((err)=>{
              console.log(err)
          })
        }
      });
}

// deleteAccount

exports.deleteAccount = (req, res) => {
    Account.findByIdAndDelete(req.body.id)
      .then(() => {
        res.status(200).json({ status: "ok", msg: "account_deleted" });
      })
      .catch((err) => {
        res.status(400).json({ status: "error", error: err });
      });
  };


  // addUserToAccount

  exports.addUserToAccount = (req,res)=>{
    console.log(req.body)// obj: { accountId: '5ff20fb42d86de4a93282d12', userEmail: 't@t.com' }

    const newobj={
      accountId:req.body.obj.accountId,
      addUserEmail:req.body.obj.userEmail,
      
    }

    User.find({email:newobj.addUserEmail}).then((data)=>{
      console.log(data)
      if(data.length===0){
        return res.json({ status: "error", msg: "Email Not Registered" });
      }
        Account.findById(newobj.accountId).then((account)=>{
          if(!account){
            return res.json({ status: "error", msg: "Account not found!" });
          }
          if (account.addUser.indexOf(newobj.addUserEmail) !== -1) {
            return res.json({
              status: "error",
              msg: "Already Added!",
            });
          }
          account.addUser.push(newobj.addUserEmail);
          account.save().then((data)=>{
            res.status(200).json({ status: "ok", msg: "User Added!" });
          }).catch((err)=>{
            console.log(err);
            res.json({
              status: "error",
              msg: "User Not  Added!",
            });
          })
        })
      
    })
    
  }

  //  getAddedUser

  exports.getAddedUser = (req,res)=>{
    const userId = req.body.userId;
    Account.find({userId:userId}).then((accounts)=>{
      //console.log(accounts)
      if(accounts.length === 0){
        return res.json({
          status: "error",
          msg: "No Account Found",
        });
      }
      let resultArr = [];
      for(let acc of accounts){
        // console.log(acc.userId)
          
           // console.log(userEmail)
           User.findById(acc.userId).then((user)=>{
              //console.log(user)
              resultArr.push({
                userId:user._id,
                userName:user.name,
                accountId:acc._id,
                accountName:acc.name,
                addedUser:acc.addUser
              });
           });
          
      }
      setTimeout(()=>{
        console.log("return");
        res.json({
          status:"ok",
          UserData:resultArr
        });
      },1500)
    }).catch((err)=>{
      console.log(err)
    })
  }